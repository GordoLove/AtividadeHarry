//
//  ViewController.swift
//  AtividadeHarry
//
//  Created by COTEMIG on 03/11/22.
//

import UIKit
import Alamofire
import  Kingfisher

struct HarryPotter: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var lista:[HarryPotter] = []
    
    var userDefaults = UserDefaults.standard
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itens = TableView.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MinhaCell
        let retorno = lista[indexPath.row]
        
        itens.nomeimg.text = retorno.name
        itens.autorimg.text = retorno.actor
        itens.imagens.kf.setImage(with: URL(string: retorno.image))
        
        return itens
    }
    
    func Novo() {
            AF.request("https://hp-api.onrender.com/api/characters").responseDecodable(of: [HarryPotter].self) { response in
                if let api = response.value {
                    self.lista = api
                    print(api)
                }
                self.TableView.reloadData()
            }
        }
    
    
    
    
    @IBOutlet weak var TableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TableView.dataSource = self
        Novo()
        
    }
    
    
}

